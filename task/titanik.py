import pandas as pd
import numpy as np


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    """
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    """
    num_of_mr = 0
    mr_nan = 0
    mr_age = []
    num_of_mrs = 0
    mrs_nan = 0
    mrs_age = []
    num_of_miss = 0
    miss_nan = 0
    miss_age = []
    for row in df.itertuples():
        print(getattr(row, "Name"))
        if getattr(row, "Name").find("Mr.") != -1:
            num_of_mr += 1
            if pd.isnull(getattr(row, "Age")):
                mr_nan += 1
            else:
                mr_age.append(getattr(row, "Age"))
        elif getattr(row, "Name").find("Mrs.") != -1:
            num_of_mrs += 1
            if pd.isnull(getattr(row, "Age")):
                mrs_nan += 1
            else:
                mrs_age.append(getattr(row, "Age"))
        elif getattr(row, "Name").find("Miss.") != -1:
            num_of_miss += 1
            if pd.isnull(getattr(row, "Age")):
                miss_nan += 1
            else:
                miss_age.append(getattr(row, "Age"))
        else:
            pass
    return [
        ('Mr.', mr_nan, int(np.round(np.median(mr_age), 0))),
        ('Mrs.', mrs_nan, int(np.round(np.median(mrs_age), 0))),
        ('Miss.', miss_nan, int(np.round(np.median(miss_age), 0))),
    ]
